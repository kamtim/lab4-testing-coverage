package com.hw.db.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ThreadDAOTests {
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void buildStubs() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
    }

    @Test
    void test1() {
        ThreadDAO.treeSort(0,null, null, true);

        Mockito
            .verify(mockJdbc)
            .query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch DESC ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any(Object.class)
            );
    }

    @Test
    void test2() {
        ThreadDAO.treeSort(0,null, null, false);

        Mockito
            .verify(mockJdbc)
            .query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any(Object.class)
            );
    }
}
