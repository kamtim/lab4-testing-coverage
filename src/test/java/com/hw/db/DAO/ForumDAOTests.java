package com.hw.db.DAO;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;

public class ForumDAOTests {
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void buildStubs() {
        mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
    }

    @Test
    void test1() {
        ForumDAO.UserList("slug",10, "08.02.2021", true);

        Mockito.verify(mockJdbc)
            .query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
            );
    }

    @Test
    void test2() {
        ForumDAO.UserList("slug",null, "08.02.2021", false);

        Mockito.verify(mockJdbc)
            .query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
            );
    }

    @Test
    void test3() {
        ForumDAO.UserList("slug",null, null, null);

        Mockito.verify(mockJdbc)
            .query(
                Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class)
            );
    }
}
