package com.hw.db.DAO;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDAOTests {
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void PreTestBehaviour() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
    }

    @Test
    void ThreadListTest1() {
        UserDAO.Change(
            new User(
                "kamtim",
                null,
                null,
                null
            )
        );

        Mockito.verifyNoInteractions(mockJdbc);
    }

    @Test
    void ThreadListTest2() {
        UserDAO.Change(
            new User(
                "kamtim",
                null,
                null,
                "about"
            )
        );

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class),
                Mockito.any(String.class)
            );
    }

    @Test
    void ThreadListTest3() {
        UserDAO.Change(
            new User(
                "kamtim",
                null,
                "fullname",
                null
            )
        );

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class),
                Mockito.any(String.class)
            );
    }

    @Test
    void ThreadListTest4() {
        UserDAO.Change(
            new User(
                "kamtim",
                "email",
                null,
                null
            )
        );

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class),
                Mockito.any(String.class)
            );
    }

    @Test
    void ThreadListTest5() {
        UserDAO.Change(
            new User(
                "kamtim",
                null,
                "fullname",
                "about"
            )
        );

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class),
                Mockito.any(String.class),
                Mockito.any(String.class)
            );
    }

    @Test
    void ThreadListTest6() {
        UserDAO.Change(
            new User(
                "kamtim",
                "email",
                "fullname",
                null
            )
        );

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class),
                Mockito.any(String.class),
                Mockito.any(String.class)
            );
    }

    @Test
    void ThreadListTest7() {
        UserDAO.Change(
            new User(
                "kamtim",
                "email",
                null,
                "about"
            )
        );

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class),
                Mockito.any(String.class),
                Mockito.any(String.class)
            );
    }

    @Test
    void ThreadListTest8() {
        UserDAO.Change(
            new User(
                "kamtim",
                "email",
                "fullname",
                "about"
            )
        );

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.any(String.class),
                Mockito.any(String.class),
                Mockito.any(String.class),
                Mockito.any(String.class)
            );
    }

}
