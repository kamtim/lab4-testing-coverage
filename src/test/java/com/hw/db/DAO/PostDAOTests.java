package com.hw.db.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

public class PostDAOTests {
    private JdbcTemplate mockJdbc;

    @BeforeEach
    void buildStubs() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);
    }

    @Test
    void test1() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc
                .queryForObject(
                    Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                    Mockito.any(PostDAO.PostMapper.class),
                    Mockito.eq(1)
                )
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1)
            );
        Mockito.verifyNoMoreInteractions(mockJdbc);
    }

    @Test
    void test2() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "not-kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1))
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
            );
    }

    @Test
    void test3() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "kamtim",
            new Timestamp(1),
            "forum",
            "message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1)
            )
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
            );
    }

    @Test
    void test4() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "another-message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1)
            )
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
            );
    }

    @Test
    void test5() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "not-kamtim",
            new Timestamp(1),
            "forum",
            "message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1)
            )
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Object.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
            );
     }

    @Test
    void test6() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "kamtim",
            new Timestamp(1),
            "forum",
            "another-message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1)
            )
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Object.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
            );
    }

    @Test
    void test7() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "not-kamtim",
            new Timestamp(0),
            "forum",
            "another-message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1)
            )
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.any(Object.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
            );
    }

    @Test
    void test8() {
        Post oldPost = new Post(
            "kamtim",
            new Timestamp(0),
            "forum",
            "message",
            0,
            0,
            false
        );
        Post newPost = new Post(
            "not-kamtim",
            new Timestamp(1),
            "forum",
            "another-message",
            0,
            0,
            false
        );

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1)
            )
        ).thenReturn(oldPost);

        PostDAO.setPost(1, newPost);

        Mockito
            .verify(mockJdbc)
            .update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.any(Object.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class),
                Mockito.any(Object.class)
            );
   }
}
